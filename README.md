# Función empty-manager

## Instalación via composer

```bin
composer require tinyphp-function/empty-manager
```

## Funciones

```php
/** Valida si el parametro enviado está vacío realmente */
function is_empty(mixed $val): bool

/** Obtener un valor por defecto en caso se detecte que el primer valor se encuentra vacío */
function def_empty(mixed $val, mixed ...$vals): mixed

/** Ejecutar una función si detecta que el valor no está vacío */
function non_empty(mixed $val, callable $cbk, mixed $def): mixed
```
