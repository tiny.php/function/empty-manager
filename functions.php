<?php
/**
 * functions.php
 * /
 * 
 * @package tinyphp-function/empty-manager
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2024, J&S Perú <https://jys.pe>
 * @created 2024-11-15 19:36:24
 * @version 20241115193655 (Rev. 6)
 * @license MIT
 * @filesource
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

if (!function_exists('is_empty'))
{
    /**
     * Valida si el parametro enviado está vacío realmente
     *
     * Casos específicos:
     * - Si es ARRAY entonces valida que tenga algún elemento para retornar TRUE
     * - Si es BOOL entonces retorna FALSO ya que es un valor así sea FALSO
     * 
     * @param mixed $val
     * @return bool
     */
    function is_empty(mixed $val): bool
    {
        $type = gettype($val);

        if ($type === 'NULL')
            return true;

        if ($type === 'string')
        {
            if ($val === '0')
                return false;

            return empty($val);
        }

        if ($type === 'array')
            return count($val) === 0;

        return false;
    }
}

if (!function_exists('def_empty'))
{
    /**
     * Obtener un valor por defecto en caso se detecte que el primer valor se encuentra vacío
     * 
     * ```php
     * # Ejemplo:
     * $val = def_empty('', function(){ return ''; }, null, [], 'texto retornado'); // Resultado: $val = 'texto retornado';
     * ```
     * 
     * @param mixed $val
     * @param mixed $val2
     * @param mixed $val3
     * @param mixed $val_n
     * @return mixed
     */
    function def_empty(mixed $val, mixed ...$vals): mixed
    {
        array_unshift($vals, $val);

        foreach ($vals as $val)
        {
            if (is_callable($val) and !is_string($val))
                $val = $val();

            if (!is_empty($val))
                return $val;
        }

        return null;
    }
}

if (!function_exists('non_empty'))
{
    /**
     * Ejecutar una función si detecta que el valor no está vacío
     * @param mixed $val
     * @param callable $cbk
     * @param mixed $def
     * @return mixed
     */
    function non_empty(mixed $val, callable $cbk, mixed $def): mixed
    {
        if (!is_empty($val))
            return $cbk($val);

        return $def;
    }
}
